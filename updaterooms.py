import re, os
print('This will update group and id of all the rooms in this directory.')
group = int(input('Type the group number of the highest vanilla room: '))
id = int(input('Type the id of the highest vanilla room: '))
for directory, subdirectories, files in os.walk('.'):
    oldgroup = ''
    for file in files:
        if file.endswith('.json'):
            path = os.path.join(directory, file)
            temp = path[2:path.index('\\', 2)]
            id += 1
            if temp != oldgroup:
                group += 1
                oldgroup = temp
                print('Group id ' + str(group))
            print('Editing ' + path)
            with open(path, 'r' ) as f:
                content = f.read()
            content_new = re.sub(r'"group": ..', '"group": ' + str(group), content, flags = re.M)
            content_new = re.sub(r'"__separator_group_ID": ..', '"__separator_group_ID": ' + str(group), content_new, flags = re.M)
            content_new = re.sub(r'"__original_Index": ....', '"__original_Index": ' + str(id), content_new, flags = re.M)
            open(path, 'w').write(content_new)
