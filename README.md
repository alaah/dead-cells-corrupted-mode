![screenshot](https://steamuserimages-a.akamaihd.net/ugc/940573211728583056/12A967331D5F1FBF0A4212C753796CAECF1D5923/)

# Howto
This is suited for [barebones](https://gitlab.com/alaah/dead-cells-barebones).

# License
See LICENSE for information.
Files inside CDB dir are based on absolutely proprietary files and their licensing is grey. I consider my "changes" to JSONs to be GPL. No one will care anyway.
PAK files belong to Motion Twin except for prisonstart_loop (Chimera by disasterpeace).