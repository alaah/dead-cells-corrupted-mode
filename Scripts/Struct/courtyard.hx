var g_mirror = "courtyard_m";
var g_exit = "izeras";

function buildMainRooms()
{
    trace("Starting Desolated Courtyard.");

    trace("Generating main rooms.");

    Struct.createSpecificRoom("aCEntrance").setName("start").addFlag(RoomFlag.Outside)
        .chain(Struct.createSpecificExit(g_mirror, "aCMidExit").addFlag(RoomFlag.Outside))
        .chain(Struct.createSpecificExit(g_exit, "aCEndExit").addFlag(RoomFlag.Outside))
        .chain(Struct.createSpecificRoom("aCEndRoom").addFlag(RoomFlag.Outside));

    trace("Generating fixed rooms.");

    Struct.createShop(Random.isBelow(0.5) ? MerchantType.Weapons : MerchantType.Actives)
        .setName("shop1")
        .addBetween("start", g_exit);
    Struct.createRoomWithType("BreakableGroundGate")
        .addBefore("shop1");

    if(Random.isBelow(0.5)) {
        Struct.createRoomWithType("Treasure")
            .addBetween("start", g_exit);
    }
    if(Random.isBelow(0.2)) {
        Struct.createRoomWithType("Treasure")
            .addBetween("start", g_exit);
    }

    Struct.createSpecificRoom("aCLadderGate")
        .addFlag(RoomFlag.Outside)
        .addBetween("start", g_exit, 2);
    Struct.createSpecificRoom("aCMultiTreasure")
        .addFlag(RoomFlag.Outside)
        .addBetween("start", g_exit);
    Struct.createRoomWithTypeFromGroup("MultiTreasure", "alaahCourtyardCommon")
        .addFlag(RoomFlag.Outside)
        .addBetween("start", g_exit);
    Struct.createRoomWithTypeFromGroup("HealPotion", "alaahCourtyardCommon")
        .addFlag(RoomFlag.Outside)
        .addBetween("start", g_exit);
    Struct.createRoomWithTypeFromGroup("CursedTreasure", "alaahCourtyardCommon")
        .addFlag(RoomFlag.Outside)
        .addBetween("start", g_exit);
    Struct.createRoomWithTypeFromGroup("BuyableTreasure", "alaahCourtyardCommon")
        .addFlag(RoomFlag.Outside)
        .addBetween("start", g_exit);
    Struct.createSpecificRoom("aCEvilRoom")
        .addFlag(RoomFlag.Outside)
        .addBetween("start", g_exit, 2);
    Struct.createSpecificRoom("aCEvilRoomEvil")
        .addFlag(RoomFlag.Outside)
        .addBetween("start", g_exit, 2);

    var exit = Struct.getRoomByName(g_exit);
    var undergrounds = Struct.allRooms.filter(function(_room)
        return _room != exit && _room.isMainLevel() && !_room.isParentOf(exit) && !_room.isChildOf(exit)
    );
    var outRooms = Struct.allRooms.filter(function(_room)
        return _room.isMainLevel() && _room.parent != null && _room.isParentOf(exit) && _room != exit
    );

    trace("Generating celldoors.");

    Struct.createRunicZDoor(Struct.createRoomWithType("Treasure"), 1, undergrounds);
    Struct.createRunicZDoor(Struct.createRoomWithTypeFromGroup("Special", "alaahCourtyardKey"), 2, undergrounds);
    Struct.createRunicZDoor(Struct.createRoomWithType("Treasure"), 3, undergrounds);
    Struct.createRunicZDoor(Struct.createRoomWithType("CursedTreasure"), 4, undergrounds);
    Struct.createRunicZDoor(Struct.createRoomWithTypeFromGroup("Special", "alaahCourtyardKey"), 4, undergrounds);
    Struct.createRunicZDoor(Struct.createSpecificRoom("GenericZDoor_LR"), 4, undergrounds);

    trace("Generating more fixed rooms.");

    Struct.createSpecificRoom("aCSecretWallJump")
        .addBefore(Random.arraySplice(outRooms).getName())
        .addFlag(RoomFlag.Outside);
    Struct.createSpecificRoom("aCSecretStomp")
        .addBefore(Random.arraySplice(outRooms).getName())
        .addFlag(RoomFlag.Outside);
    Struct.createSpecificRoom("aCSecretHouse")
        .addBefore(Random.arraySplice(outRooms).getName())
        .addFlag(RoomFlag.Outside);
    Struct.createSpecificRoom("aCSecretFlower")
        .addBefore(Random.arraySplice(outRooms).getName())
        .addFlag(RoomFlag.Outside);
    Struct.createSpecificRoom("aCIsland")
        .addBefore(Random.arraySplice(outRooms).getName())
        .addFlag(RoomFlag.Outside)
        .addZChild(Struct.createSpecificRoom("aCIslandInt"))
        .setName("desolate");
}

function buildSecondaryRooms()
{
    var exit = Struct.getRoomByName(g_exit);

    var mains = Struct.allRooms.filter(function(_room)
        return _room.isMainLevel() && _room.parent != null && _room.isParentOf(exit) && _room != exit
    );

    trace("Generating combat rooms.");
    var mainsCopy = mains.copy();
    for(i in 0...4) {
        if(mainsCopy.length == 0) {
            mainsCopy = mains.copy();
        }
        Struct.createRoomWithTypeFromGroup("Combat", "alaahCourtyardCommon")
            .addFlag(RoomFlag.Outside)
            .addBefore(Random.arraySplice(mainsCopy).getName());
    }

    var combatRooms = Struct.allRooms.filter(function(_room)
        return _room.type == "Combat" && _room.hasFlag(RoomFlag.Outside)
    );
    var undergrounds = Struct.allRooms.filter(function(_room)
        return _room != exit && _room.isMainLevel() && !_room.isParentOf(exit) && !_room.isChildOf(exit)
    );

    trace("Generating trap rooms.");
    for(i in 0...Random.irange(2, 3)){
        Struct.createRoomWithTypeFromGroup("Trap_1", "alaahCourtyardCommon")
            .addFlag(RoomFlag.Outside)
            .addBefore(Random.arraySplice(combatRooms).getName());
    }
    Struct.createRoomWithTypeFromGroup("Combat", "alaahCourtyardKey")
        .addFlag(RoomFlag.Outside)
        .addBefore(Random.arraySplice(combatRooms).getName());
    for(i in 0...Random.irange(3, 5)) {
        Struct.createRoomWithTypeFromGroup("Trap_1", "CommonTraps")
            .addBefore(Random.arrayPick(undergrounds).getName());
    }
}

function addTeleports()
{
    var exit = Struct.getRoomByName(g_exit);
    var secondExit = Struct.getRoomByName(g_exit);
    var rooms = Struct.allRooms.filter(function(_room)
        return _room.isMainLevel()
    );
    var mains = Struct.allRooms.filter(function(_room)
        return _room.isMainLevel() && _room.parent != null && _room.isParentOf(exit)
    );

    trace("Generating TPs.");
    for(room in rooms) {
        if(room.type == "Corridor" && room.isParentOf(exit) && room.childrenCount > 1 && room.calcTypeDistance("Teleport", true) > 1) {
            room.setType("Teleport");
        }
    }

    for(room in rooms.filter(function(_room) return !_room.hasFlag(RoomFlag.Outside) && (_room.type == "WallJumpGate" || _room.type == "LadderGate" || _room.type == "TeleportGate" || _room.type == "BreakableGroundGate")) ){
        if(room.calcTypeDistance("Teleport", false) > 1 ){
            Struct.createTeleportBefore(room);
        }
    }

    for(room in rooms) {
        if(room.childrenCount == 0 && room != secondExit && room.calcTypeDistance("Teleport", true) > 1){
            Struct.createRoomWithType("Teleport").addBefore(room.getName());
        }
    }

    if(exit.parent.type != "Teleport") {
        Struct.createRoomWithTypeFromGroup("Teleport", "alaahCourtyardCommon").addFlag(RoomFlag.Outside).addBefore(exit.getName());
    }

    var nodes = rooms.filter(function(_room)
        return _room.isMainLevel() && _room.hasFlag(RoomFlag.Outside)
    );
    nodes.sort(function(a, b) return compare(a.spawnDistance, b.spawnDistance));
    for(n in nodes) {
        if(n.spawnDistance > 1 && n.parent != null && n.calcTypeDistance("Teleport", true) > 2) {
                Struct.createRoomWithTypeFromGroup("Teleport", "alaahCourtyardCommon").addFlag(RoomFlag.Outside).addBefore(n.getName());
        }
    }
}

function buildTimedDoors() {}

function finalize()
{
    var exit = Struct.getRoomByName(g_exit);

    trace("Finalizing.");
    for(room in Struct.allRooms){
        if((room.type == "Corridor" || room.type == "Teleport") && room.isParentOf(exit)){
            room.setGroup("alaahCourtyardCommon");
            room.addFlag(RoomFlag.Outside);
        }
    }

    for(room in Struct.allRooms) {
        if(room.parent != null && room.hasFlag(RoomFlag.Outside)) {
            room.setConstraint(LinkConstraint.HorizontalSameDirOnly);
            room.setChildPriority(1);
        }
    }

    Struct.getRoomByName("start").addChild(Struct.createSpecificExit("BoatDock", "aCStartExit"));

    trace("Done.");
}
function buildMobRoster(_mobList)
{
    trace("Applying mob roster.");
    addMobRosterFrom("PrisonCourtyard", _mobList);
}

function setLevelProps(_levelProps)
{
    trace("Applying props.");
    _levelProps.wind = -3.0;
    _levelProps.musicIntro = "music/courtyard_intro.ogg";
    _levelProps.musicLoop = "music/courtyard_loop.ogg";
    _levelProps.zDoorColor = 144411;
}

function setLevelInfo(_levelInfo)
{
    trace("Applying level info.");
    _levelInfo.baseLootLevel = 2;
    _levelInfo.baseMobTier = 2;
    _levelInfo.biome = "PrisonCourtyard2";
    _levelInfo.cellBonus = 0.0;
    _levelInfo.doubleUps = 0;
    _levelInfo.eliteRoomChance = 0.0;
    _levelInfo.eliteWanderChance = 1.0;
    _levelInfo.extraMobTier = 0;
    _levelInfo.flags = 13;
    _levelInfo.goldBonus = 0;
    _levelInfo.mobDensity = 3.0;
    _levelInfo.name = "Desolated Courtyard";
    _levelInfo.tripleUps = 2;
}
