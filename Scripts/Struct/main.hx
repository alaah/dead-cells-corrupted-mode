var g_exit = "courtyard";

function buildMainRooms() {
    Struct.createRoomWithType("Entrance").setName("start")
    .chain(Struct.createSpecificRoom("aPStarters").setName("starters"))
    .chain(Struct.createSpecificRoom("aPFlaskRoom").setName("flask"))
    .chain(Struct.createSpecificExit(g_exit, "aPExit").setTitleAndColor("Desolated Courtyard", 144411));

    Struct.getRoomByName("flask").addChild(Struct.createSpecificRoom("aPFogger").setName("special_fogger"));
    Struct.createSpecificRoom("aPInvisPuzzle").addAfter("special_fogger");

    Struct.createSpecificRoom("aPCursedSword").branchBetween("starters", "flask").setName("special_cs");
    Struct.createSpecificRoom("aPTreasure").branchBetween("starters", "flask").setName("special_t");

    for(i in 0...5) {
        Struct.createRoomWithTypeFromGroup("Combat", "alaahPrison").addBetween("starters", "flask");
    }
    for(i in 0...3) {
        Struct.createRoomWithTypeFromGroup("Combat", "alaahPrison").addBetweenMultipleEnds("starters", ["special_cs", "special_t"]);
    }
}

function buildSecondaryRooms() {

}

function addTeleports() {
    var rooms = Struct.allRooms.filter(function(room) {
        return room.type == "Combat" && room.name != "aPFlaskRoom";
    });

    for(room in rooms) {
        if(room.childrenCount >= 1 && room.calcTypeDistance("Teleport", true) > 3) {
            Struct.createTeleportBefore(room);
        }
    }
}

function buildTimedDoors() {}

function finalize() {
    if(Meta.hasMetaRune("LadderKey")) {
        Struct.createSpecificRoom("aPLock").addAfter("starters");
    } else if(!Meta.hasMetaRune("WallJumpKey")) {
        Struct.createSpecificRoom("aPWallJump").addAfter("starters");
    }
}

function buildMobRoster(_mobList) {
    addMobRosterFrom("PrisonStart", _mobList);
}

function setLevelProps(_levelProps) {
    setLevelPropsFrom("PrisonStart", _levelProps);
    _levelProps.wind = 0.0;
    _levelProps.musicIntro = "music/prisonstart_loop.ogg";
    _levelProps.musicLoop = "music/prisonstart_loop.ogg";
}

function setLevelInfo(_levelInfo) {
    setLevelInfoFrom("PrisonStart", _levelInfo);
    _levelInfo.baseLootLevel = 1;
    _levelInfo.baseMobTier = 1;
    _levelInfo.biome = "PrisonStart";
    _levelInfo.cellBonus = 0.0;
    _levelInfo.doubleUps = 0;
    _levelInfo.eliteRoomChance = 0;
    _levelInfo.eliteWanderChance = 0;
    _levelInfo.extraMobTier = 0;
    _levelInfo.flags = 1;
    _levelInfo.goldBonus = 0;
    _levelInfo.mobDensity = 0.7;
    _levelInfo.name = "Decayed Quarters";
    _levelInfo.tripleUps = 2;
}
