function buildMainRooms()
{
    Struct.createSpecificRoom("PrisonFlaskRoom").setName("start")
    .chain(Struct.createSpecificRoom("PerkShop").setName("perk"))
    .chain(Struct.createSpecificRoom("PrisonMonsterDoor").setName("monster"))
    .chain(Struct.createSpecificExit("T_PrisonDepths", "aPDangerousExit").setName("mainExit"));

    Struct.createRoomWithType("BuyableTreasure")
        .addBefore("mainExit");
    Struct.createRoomWithType("WallJumpGate")
        .addBefore("mainExit");
    Struct.createSpecificRoom("ChristmasGift")
        .addBefore("mainExit");
    Struct.createSpecificRoom("Pokeball")
        .addBefore("mainExit");
    Struct.createSpecificRoom("aPMoreEasyGetItem")
        .branchBetween("monster", "mainExit")
        .setName("moreeasy");

    Struct.createShop().branchBetween("monster", "mainExit");
    Struct.createRoomWithType("Treasure").branchBetween("monster", "mainExit");
    Struct.createRunicZDoor(Struct.createRoomWithType("Treasure"), 3, Struct.allRooms);
    Struct.createRunicZDoor(Struct.createRoomWithType("Treasure"), 1, Struct.allRooms);
}

function buildSecondaryRooms()
{
    trace("buildSecondaryRooms");
    var exit = Struct.getRoomByName("mainExit");
    var mains = Struct.allRooms.filter(function(_room) return _room.parent != null &&
    _room.parent != Struct.getRoomByName("start") &&
    _room.parent != Struct.getRoomByName("perk") &&
    _room.parent != Struct.getRoomByName("monster") &&
    _room != exit && _room.isMainLevel() && _room.isParentOf(exit) && _room.childrenCount > 1);
    var mainsCopy = mains.copy();
    for(i in 0...Random.irange(1, 4)) {
        if(mainsCopy.length == 0) {
            mainsCopy = mains.copy();
        }
        Struct.createRoomWithTypeFromGroup("Combat", "Prison").addBefore(Random.arraySplice(mainsCopy).getName());
    }
    for(i in 0...2) {
        Struct.createRoomWithTypeFromGroup("Combat", "Prison").addBefore("mainExit");
    }
}

function addTeleports()
{
    var rooms = Struct.allRooms.filter(function(_room) return _room.isMainLevel() );
    var exit = Struct.getRoomByName("mainExit");

    for(room in rooms) {
        if( room.type == "Corridor" && room.childrenCount > 1 && room.calcTypeDistance("Teleport", true) > 1 ){
            room.setType("Teleport");
        }
    }
    for(room in rooms) {
        if(room.calcTypeDistance("Teleport", true) > 2){
            Struct.createRoomWithType("Teleport").addBefore(room.getName());
        }
    }
}

function buildTimedDoors()
{
    var dh = new DecisionHelper(Struct.allRooms);
    dh.score(function(_room) return _room.spawnDistance <= 5 ? -4 : _room.spawnDistance >= 8 ? -10 : 0);
    dh.score(function(_room) return Random.irange(0, 1));

    Struct.createTimedBranchBefore(dh.getBest());
}

function finalize()
{
    Struct.createRoomWithType("TeleportGate")
        .addBefore("mainExit");

    Struct.createSpecificRoom("aPRollZone")
        .addBefore("moreeasy");
}
function buildMobRoster(_mobList)
{
    addMobRosterFrom("PrisonStart", _mobList);
}

function setLevelProps(_levelProps)
{
    setLevelPropsFrom("PrisonStart", _levelProps);
    _levelProps.timedDoor = 8.0;
    _levelProps.timedGoldMul = 2.0;
    _levelProps.timedScrolls = 1;
    _levelProps.wind = 0.0;
    _levelProps.musicIntro = "music/prisonstart_loop.ogg";
    _levelProps.musicLoop = "music/prisonstart_loop.ogg";

    _levelProps.doorColor = 11278336;
    _levelProps.zDoorColor = 11278336;
    _levelProps.loadingColor = 2491170;
    _levelProps.loadingDescColor = 6652612;
}

function setLevelInfo(_levelInfo){
    setLevelInfoFrom("PrisonStart", _levelInfo);
    _levelInfo.baseLootLevel = 3;
    _levelInfo.baseMobTier = 5;
    _levelInfo.biome = "PrisonStart";
    _levelInfo.cellBonus = 0.0;
    _levelInfo.doubleUps = 1;
    _levelInfo.eliteRoomChance = 1.0;
    _levelInfo.eliteWanderChance = 1.0;
    _levelInfo.extraMobTier = 0;
    _levelInfo.flags = 1;
    _levelInfo.goldBonus = 0;
    _levelInfo.mobDensity = 1.3;
    _levelInfo.name = "Decaying Cells";
    _levelInfo.tripleUps = 2;
}
